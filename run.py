import os
from src.app.server import create_app

if __name__ == '__main__':
    # config_name = os.getenv('FLASK_ENV')
    # config_name = os.getenv('APP_SETTINGS')
    app = create_app()
    app.run(host="localhost", port=3000, debug=True)